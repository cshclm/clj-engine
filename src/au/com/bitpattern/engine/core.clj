(ns au.com.bitpattern.engine.core
  "Implementation of engines."
  (:require
   [clojure.core.async :refer [go go-loop <!! >!! put! alts! chan]]))

(defn- make-process [f]
  "Execute `f' for `ticks' iterations, providing the outcome on an channel."
  (fn [ticks state ch]
    (trampoline (fn rec [ticks state]
                  (try
                    (let [{:keys [next-state result]} (apply f state)]
                      (cond
                        result (>!! ch {:type :result :message result})
                        (= ticks 1) (>!! ch {:type :suspend :message next-state})
                        :default (fn [] (rec (dec ticks) next-state))))
                    (catch Throwable e
                      (>!! ch {:type :error
                               :message {:error e
                                         :state state}}))))
                ticks state)))

(defn make-engine
  "Create an engine for the given function.

An engine is a function which given a quantity of work to do, and callbacks on
complete and expire, will try to make progress towards its goal.  If it
completes, it will call `complete' with its returned value, otherwise it will
call expire with an engine which can be used to continue the computation.

Computation of `f' will be performed in another thread.

The function `f' must meet the following criteria:

  - Has a zero argument form which initiates the computation,
  - Should always return a map,
  - The returned map map must contain the key :result with the final value
    once the computation is complete.
  - Should return a map with the key :next-state if the computation is not
    complete, whose value is an vector of the arguments to which the function
    should next be applied.

`complete' is a function taking one argument which will be invoked when the
computation is complete.  The argument is the result of the computation of
`f'.

`expire' is a function taking 1 argument, which is an engine which can be
called to continue the computation.  The engine takes the number of ticks to
execute for as its argument."
  ([f complete suspend] (make-engine f complete suspend nil))
  ([f complete suspend error]
   (let [ch (chan)
         result-ch (chan)
         process (make-process f)]
     (letfn [(eng [next-state ticks]
               (go
                 (process ticks next-state ch))
               nil)]
       (go-loop []
         (let [[v p] (alts! [ch result-ch])]
           (if (= p result-ch)
             (complete v)
             (let [{:keys [type message]} v]
               (condp = type
                 :result (complete message)
                 :suspend (suspend {:channel result-ch
                                    :engine eng
                                    :state message})
                 :error (when (fn? error)
                          (error {:channel result-ch
                                  :engine eng
                                  :state (:state message)
                                  :error (:error message)}))))))
         (recur))
       (partial eng nil)))))

(defn make-sync-engine
  "Create a synchronous (blocking) engine for the given function.

  Unlike `make-engine' however this is blocking and the final result is the
  return value of this function."
  ([f suspend] (make-sync-engine f suspend nil))
  ([f suspend error]
   (let [result-ch (chan)
         complete-fn #(put! result-ch %)
         eng (make-engine f complete-fn suspend error)]
     (fn [steps]
       (eng steps)
       (let [r (<!! result-ch)]
         (if (instance? Throwable r)
           (throw r)
           r))))))

(defn- rewrite-form
  [fn-name form]
  (cond
    (and (seq? form) (= (first form) fn-name))
    {:next-state (into [] (rest form))}

    (and (seq? form) (some #{(first form)} '(do let*)))
    (concat (take (dec (count form)) form) (list (rewrite-form fn-name (last form))))

    (and (seq? form) (= (first form) 'if))
    (concat (take 2 form) (map (partial rewrite-form fn-name) (drop 2 form)))

    :default
    {:result form}))

(defmacro unwind-tail [outer-args [fn-builtin inner-name & definitions]]
  "Unwinds a tail-recursive function, making it suitable for use with an engine.

`outer-name' is the name which the new function should be exposed as, and
outer-args is the arguments for that function. The third argument is a
function definition, with both zero-argument and n-argument forms.

Example usage:

  (unwind-tail factorial [n]
               (fn -fac
                 ([] (-fac n (dec n)))
                 ([a n] (if (= n 1)
                          a
                          (-fac (* a n) (dec n))))))

((make-engine (factorial 6)
  #(println %)
  (fn [e] (e 2)))
 2)

;; => nil
;; 720"
  `(fn ~outer-args
     (fn ~inner-name
       ~(first (filter #(empty? (first %)) definitions))
       ~(let [[inner-args# & body#] (first (filter #(seq (first %)) definitions))]
          (apply list inner-args#
                (into '() (conj (into [] (take (dec (count body#)) body#))
                                (rewrite-form inner-name (clojure.walk/macroexpand-all
                                                          (last body#))))))))))
