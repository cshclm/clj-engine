(ns au.com.bitpattern.engine.core-test
  (:require [clojure.test :refer :all]
            [au.com.bitpattern.engine.core :as sut]
            [clojure.core.async :refer [put! chan timeout alts!!]]))

(defn- count-to
  [n]
  (fn -count-to
    ([] (-count-to 1))
    ([c]
     (if (= c n)
       {:result c}
       {:next-state [(inc c)]}))))

(defn- factorial
  [n]
  (fn -fac
    ([] (-fac {:acc 1 :r n}))
    ([{:keys [acc r]}]
     (if (= r 1)
       {:result acc}
       {:next-state [{:acc (* acc r) :r (dec r)}]}))))

(defn is-async
  "Waits for timeout `tout' for a put onto ch, which is apply to expr.
  `expr' should be a function of one argument, and will usually wrap an
  assertion."
  [tout ch expr]
  (let [t (timeout tout)
        [v p] (alts!! [ch t])]
    (if (= p ch)
      (expr v)
      (do-report
        {:type :fail,
         :message "Did not receive data on channel before timeout"}))))

(deftest test-make-engine
  (testing "make-engine"
    (testing "should fire callback with expected result if ticks is sufficient"
      (let [ch (chan)]
        ((sut/make-engine (factorial 6)
                          (fn [x] (put! ch x))
                          (fn [{:keys [engine state]}] nil)) 6)
        (is-async 1000 ch #(is (= % 720)))))

    (testing "should fire expire callback if number of tickes insufficient"
      (let [ch (chan)
            eng (sut/make-engine (factorial 6)
                                 identity
                                 (fn [{:keys [engine state]}] (put! ch engine)))]
        (eng 3)
        (is-async 1000 ch (fn [eng] (is (fn? eng))))))

    (testing "should resume from where it left off"
      (let [ch (chan)
            eng (sut/make-engine (factorial 6)
                                 (fn [x] (put! ch x))
                                 (fn [{:keys [engine state]}] (engine state 3)))]
        (eng 3)
        (is-async 1000 ch #(is (= % 720)))))

    (testing "should allow number of ticks to be varied"
      (let [ch (chan)
            eng (sut/make-engine (factorial 6)
                                 (fn [x] (put! ch x))
                                 (fn [{:keys [engine state]}] (engine state 4)))]
        (eng 2)
        (is-async 1000 ch #(is (= % 720)))))

    (testing "should allow state to be manipulated during suspend"
      (let [ch (chan)
            eng (sut/make-engine (factorial 6)
                                 (fn [x] (put! ch x))
                                 (fn [{:keys [engine state]}]
                                   (engine [(update (first state) :acc inc)] 4)))]
        (eng 2)
        (is-async 1000 ch #(is (= % 744)))))

    (testing "should try to call the error handler if an exception is thrown"
      (letfn [(dec-div [n]
                (fn -dec-div
                  ([] (-dec-div 1 n))
                  ([d n]
                   (if (= n -1)
                     {:result n}
                     {:next-state [(/ n n) (dec n)]}))))]
        (let [ch (chan)
              eng (sut/make-engine (dec-div 2)
                                   identity
                                   (fn [{:keys [engine state]}]
                                     (engine state 4))
                                   (fn [{:keys [channel engine state error]}]
                                     (put! ch [engine state error])))]
          (eng 1)
          (is-async 1000 ch (fn [[engine state error]]
                              (do
                                (is (fn? engine))
                                (is (= state [1 0]))
                                (is (instance? ArithmeticException error))))))))

    (testing "should allow error handler to correct problem and carry on"
      (letfn [(dec-div [n]
                (fn -dec-div
                  ([] (-dec-div 1 n))
                  ([d n]
                   (if (= n -3)
                     {:result d}
                     {:next-state [(/ d n) (dec n)]}))))]
        (let [ch (chan)
              eng (sut/make-engine (dec-div 2)
                                   (fn [x] (put! ch x))
                                   (fn [{:keys [engine state]}]
                                     (engine state 1))
                                   (fn [{:keys [engine state error]}]
                                     (if (instance? ArithmeticException error)
                                       (engine [(first state) (dec (second state))] 1))))]
          (eng 1)
          (is-async 1000 ch #(is (= % 1/4))))))

    (testing "should cease executing once completed, even if more ticks remain"
      (let [ch (chan)
            eng (sut/make-engine (factorial 6)
                                 (fn [x] (put! ch x))
                                 (fn [{:keys [engine state]}] nil))]
        (eng 20)
        (is-async 1000 ch #(is (= % 720)))))

    (testing "should return the same result consistently after completion"
      (let [ch (chan)
            eng (sut/make-engine (factorial 6)
                                 (fn [x] (put! ch x))
                                 (fn [{:keys [engine state]}] nil))]
        (eng 20)
        (is-async 1000 ch #(is (= % 720)))))

    (testing "shouldn't allow an engine to be executed multiple times with the same result"
      (let [ch (chan)
            eng (sut/make-engine (factorial 6)
                                 identity
                                 (fn [{:keys [engine state]}]
                                   (put! ch engine)))]
        (eng 4)
        (is-async 1000 ch (fn [x] (is (fn? x))))
        (eng 4)
        (is-async 1000 ch (fn [x] (is (fn? x))))))

    (testing "should return a new instance of the engine with each invocation"
      (let [ch (chan)
            eng (sut/make-engine (factorial 6)
                                 identity
                                 (fn [{:keys [engine state]}]
                                   (put! ch engine)))]
        (eng 4)
        (let [prev-eng (atom nil)]
          (is-async 1000 ch #(do (is (not= % eng))
                                 (reset! prev-eng eng)))
          (eng 4)
          (is-async 1000 ch #(is (not= % eng)
                                 (not= % @prev-eng))))))

    (testing "should not consume a stack frame with each tick"
      (let [ch (chan)
            eng (sut/make-engine (count-to 500000)
                                 (fn [x] (put! ch x))
                                 (fn [{:keys [engine state]}] nil))]
        (eng 500000)
        (is-async 1000 ch #(is (= % 500000)))))))

(deftest test-make-sync-engine
  (testing "make-sync-engine"
    (testing "should synchronously return calculated value"
      (let [ch (chan)
            eng (sut/make-sync-engine (factorial 6)
                                      (fn [{:keys [channel engine state]}]
                                        (engine state 3)))]
        (is (= (eng 3) 720))))

    (testing "should allow for exceptions to be propagated"
      (letfn [(dec-div [n]
                (fn -dec-div
                  ([] (-dec-div 1 n))
                  ([d n]
                   (if (= n -3)
                     {:result d}
                     {:next-state [(/ d n) (dec n)]}))))]
        (let [ch (chan)
              eng (sut/make-sync-engine (dec-div 2)
                                        (fn [{:keys [engine state]}]
                                          (engine state 1))
                                        (fn [{:keys [channel error]}]
                                          (if (instance? ArithmeticException error)
                                            (do
                                              (put! channel error)))))]
          (is (thrown? ArithmeticException (eng 1))))))

    (testing "should allow for errors to be propagated"
      (letfn [(dec-div [n]
                (fn -dec-div
                  ([] (-dec-div 1 n))
                  ([d n]
                   (if (= n -3)
                     {:result d}
                     {:next-state [(/ d n) (dec n)]}))))]
        (let [ch (chan)
              eng (sut/make-sync-engine (dec-div 2)
                                        (fn [{:keys [engine state error]}]
                                          (engine state 1))
                                        (fn [{:keys [channel engine state error]}]
                                          (if (instance? ArithmeticException error)
                                            (put! channel (UnknownError. "very unknown")))))]
          (is (thrown? UnknownError (eng 1))))))))

(deftest test-unwind-tail
  (testing "unwind-tail"
    (testing "should take a tail recursive function and make it suitable for use with an engine"
      (let [macro-fac (sut/unwind-tail [n]
                                       (fn -fac
                                         ([] (-fac n (dec n)))
                                         ([a n] (if (= n 1)
                                                  a
                                                  (-fac (* a n) (dec n))))))]
        (let [ch (chan)]
          ((sut/make-engine (macro-fac 6)
                            (fn [x] (put! ch x))
                            (fn [{:keys [engine state]}] (engine state 3))) 3)
          (is-async 1000 ch #(is (= % 720))))))

    (testing "should return expected value when recursive call is within a 'do'"
      (let [macro-fac (sut/unwind-tail [n]
                                       (fn -fac
                                         ([] (-fac n (dec n)))
                                         ([a n] (if (= n 1)
                                                  a
                                                  (do (if (= true false)
                                                        true
                                                        (do
                                                          true
                                                          (-fac (* a n) (dec n)))))))))]
        (let [ch (chan)]
          ((sut/make-engine (macro-fac 6)
                            (fn [x] (put! ch x))
                            (fn [{:keys [engine state]}] (engine state 3))) 3)
          (is-async 1000 ch #(is (= % 720))))))

    (testing "should return expected value when terminating branch is within a 'do'"
      (let [macro-fac (sut/unwind-tail [n]
                                       (fn -fac
                                         ([] (-fac n (dec n)))
                                         ([a n] (if (= n 1)
                                                  (do
                                                    true
                                                    a)
                                                  (-fac (* a n) (dec n))))))]
        (let [ch (chan)]
          ((sut/make-engine (macro-fac 5)
                            (fn [x] (put! ch x))
                            (fn [{:keys [engine state]}] (engine state 3))) 3)
          (is-async 1000 ch #(is (= % 120))))))

    (testing "should return expected value when recursive call is within a 'let'"
      (let [macro-fac (sut/unwind-tail [n]
                                       (fn -fac
                                         ([] (-fac n (dec n)))
                                         ([a n] (if (= n 1)
                                                  a
                                                  (let [nd (dec n)]
                                                    (-fac (* a n) nd))))))]
        (let [ch (chan)]
          ((sut/make-engine (macro-fac 4)
                            (fn [x] (put! ch x))
                            (fn [{:keys [engine state]}] (engine state 3))) 3)
          (is-async 1000 ch #(is (= % 24))))))))
