# Engines for clojure

An implementation of [engines](https://en.wikipedia.org/wiki/Engine_(computer_science)) for Clojure.

Based on [Literate Engines in Lisp, Sooriamurthi R., 1995.](http://www.literateprogramming.com/luv95.pdf)

### Installation

[Leiningen](https://github.com/technomancy/leiningen) dependency information:

```clj
 [org.clojure/clojure "1.8.0"]
 [au.com.bitpattern/engine "0.0.2"]
```

[Maven](http://maven.apache.org/) dependency information:

```xml
<dependency>
  <groupId>au.com.bitpattern</groupId>
  <artifactId>engine</artifactId>
  <version>0.0.2</version>
</dependency>
```

### Usage

```clj
(require '[au.com.bitpattern.engine.core :as engine])

(def factorial
  (engine/unwind-tail
   [n]
   (fn -fac
     ([] (-fac n (dec n)))
     ([a n]
      (if (= n 1)
        a
        (-fac (* a n) (dec n)))))))

(let [fact-eng (engine/make-engine (factorial 10)
                    #(println (format "Complete with result: %d" %))
                    (fn [{:keys [engine state]}]
                      (println "Evaluating...")
                      (engine state 2)))]
  (fact-eng 2))

;; => nil
;; Evaluating...
;; Evaluating...
;; Evaluating...
;; Evaluating...
;; Complete with result: 3628800
```

`unwind-tail` takes a tail-recursive function and unwinds it, so that in each
iteration it returns either the arguments for the next iteration or the result
of the evaluation.  The above usage is equivalent to:

```clj
(def factorial
  (fn [n]
    (fn -fac
      ([]
       (-fac {:acc 1 :r n}))
      ([a n]
       (if (= n 1)
         {:result a}
         {:next-state [(* a n) (dec n)]})))))
```

The goal here is to achieve bounded computation, with the caveat that each
iteration of the computation should be relatively cheap.

As an example, let's place a 100ms upper-bound on wall-clock time for finding
the nth number in a fibonacci sequence:

```clj
(def fibonacci
  (engine/unwind-tail
   [n]
   (fn -fib
     ([] (if (= n 0)
           {:result 0}
           (-fib 1 0 1)))
     ([iter prev cur]
      (if (= iter n)
        cur
        (-fib (inc iter) cur (bigint (+' prev cur))))))))

(defn run-fib-engine [n {:keys [max-wait step]}]
  (with-local-vars [start-time nil]
    (let [eng (engine/make-engine
               (fibonacci n)
               #(println (format "Complete with result: %s" %))
               (fn [{:keys [engine state]}]
                 (if (and max-wait
                     (> (- (System/currentTimeMillis) (var-get start-time))
                        max-wait))
                   (println "Abandoning expensive computation")
                   (engine state (or step 10)))))]
      (var-set start-time (System/currentTimeMillis))
      (eng (or step 10)))))

(run-fib-engine 10 {:max-wait 100 :step 10})
;; => nil
;; Complete with result: 55

(run-fib-engine 100000 {:max-wait 100 :step 10})
;; => nil
;; Abandoning expensive computation
```

For convenience, there is also a synchronously executing variant:

```clj
(let [eng (engine/make-sync-engine (factorial 6)
                                   (fn [{:keys [engine state]}]
                                     (engine state 1)))]
  (eng 1))
;; => 720
```

## License

Copyright © 2017 Chris Mann

Distributed under the Eclipse Public License either version 1.0 or (at your
option) any later version.
