(def project 'au.com.bitpattern/engine)
(def version "0.0.3-SNAPSHOT")

(set-env! :resource-paths #{"resources" "src"}
          :source-paths   #{"test"}
          :dependencies   '[[org.clojure/clojure "RELEASE"]
                            [org.clojure/core.async "0.3.443"]
                            [adzerk/boot-test "RELEASE" :scope "test"]])

(task-options!
 pom {:project     project
      :version     version
      :description "An engine implementation for Clojure."
      :scm         {:url "https://gitlab.com/cshclm/clj-engine"}
      :license     {"Eclipse Public License"
                    "http://www.eclipse.org/legal/epl-v10.html"}})

(deftask build
  "Build and install the project locally."
  []
  (comp (pom) (jar) (install)))

(deftask deploy
  "Build and install the project locally."
  []
  (comp (pom) (jar) (push)))

(require '[adzerk.boot-test :refer [test]])
